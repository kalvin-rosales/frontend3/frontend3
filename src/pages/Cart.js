import { Add, Remove } from '@material-ui/icons';
import React from 'react'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
// import Popup from 'reactjs-popup';


import styled from "styled-components";
import Announcement from '../components/Announcement'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'


const Container = styled.div`



`




const Wrapper = styled.div`
    padding: 20px;


`




const Title = styled.h1`
    font-weight: 500;
    text-align:center;


`
const Top = styled.div`
    display:flex;
    align-items:center;
    justify-content: space-between;
    padding: 20px;


`

const TopButton = styled.button`
    padding: 10px;
    font-weight:600;
    cursor: pointer;
    border: ${(props) => props.type === "filled" && "none"};
    background-color: ${(props) => props.type === "filled" ? "black" : "transparent"};
    color: ${(props) => props.type === "filled" && "white"}
`

const TopTexts = styled.div``;

const TopText = styled.span`
    text-decoration: underline;
    cursor:pointer;
    margin: 0px 10px;
`;
const Bottom = styled.div`
    display:flex;
    justify-content: space-between;



`
const Info = styled.div`
    flex:3;



`
const Product = styled.div`
    display:flex;
    justify-content: space-between;

`
const ProductDetail = styled.div`
    flex:2;
    display:flex;


`
const Image = styled.img`
    width: 200;


`
const Details = styled.div`

    padding:20px;
    display:flex;
    flex-direction: column;
    justify-content: space-around;


`
const ProductName = styled.span``
const ProductId = styled.span``
const ProductSize = styled.span``

const PriceDetail = styled.div`
    flex:1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;


`
const ProductAmountContainer = styled.div`
    display: flex;
    align-items:center;
    margin-bottom: 20px;


`
const ProductAmount = styled.div`
    font-size: 24px;
    margin: 5px;


`
const ProductPrice = styled.div`
        font-size:30px;
        font-weight:200;

`

const Hr = styled.hr`

    background-color: black;
    border: none;
    height: 2px;
`

const Summary = styled.div`
    flex: 1;
  
    padding:20px;



`
const SummaryTitle = styled.h1`
    font-weight: 200;
    text-align:center;

`
const SummaryItem = styled.div`
    margin: 30px 30px;
   
    font-weight: ${props => props.type === "total" && "500"};
    font-size: ${props => props.type === "total" && "500"};
`
const SummaryItemText = styled.span`
    margin: 30px 30px;
    font-size: 25px;
    
`
const SummaryItemPrice = styled.span`
        margin: 30px 30px;
    font-size: 25px;
    `
const Button = styled.button`
    width: 100%;
    padding: 10px;
    margin-top: 50px;
    background-color: red;
    color: white;
    transition:all 1s ease;
  &:hover{
    background-color:pink;
    transform: scale(1.5);
  }
`






const Cart = () => {

    const cart = useSelector(state => state.cart)


    const handleClickAlert = () => {
        
        
    

        alert(" Thank you very much ")
      };

  return (
    <Container>
        <Announcement />
        <Navbar />
            <Wrapper>
                <Title>YOUR ITEMS</Title>
                <Top>
                    <TopButton>CONTINUE SHOPPING</TopButton>
                    <TopTexts>
                        <TopText>Shopping Bag(2)</TopText>
                        <TopText>Your Wishlist (0)</TopText>
                    </TopTexts>
                    <TopButton type="filled">CHECKOUT NOW</TopButton>
                </Top>
                <Bottom>

                    <Info>



                    {cart.products.map(product => (

                   

                        <Product>
                            <ProductDetail>

                                <Image src={product.img}/>
                                <Details>
                                    <ProductName><b>Product:</b> {product.title} </ProductName>
                                    <ProductId><b>ProductId:</b> {product._id}
                                    </ProductId>
                                    <ProductSize><b>Size:</b> {product.size}
                                    </ProductSize>
                                   
                                </Details>
                            </ProductDetail>
                            <PriceDetail>
                                <ProductAmountContainer>
                                    <Add/>
                                        <ProductAmount>

                                            {product.quantity}
                                        </ProductAmount>
                                    <Remove/>

                                </ProductAmountContainer>
                                <ProductPrice>
                                    {
                                        product.price * product.quantity
                                    }
                                </ProductPrice>
                            </PriceDetail>
                        </Product>

                        ))}

                        <Hr/>
                    
                      


                    </Info>
                    
                    <Summary>
                        <SummaryTitle>
                            ORDER SUMMARY
                        </SummaryTitle>
                        <SummaryItem type= "total">
                            <SummaryItemText >Total</SummaryItemText>
                            <SummaryItemPrice>${cart.total}</SummaryItemPrice>
                            <Link to="/">
                            <Button onClick={handleClickAlert}>CHECKOUT NOW</Button>
                                    
                            </Link>
                        </SummaryItem>
                    </Summary>
                </Bottom>
            </Wrapper>
        <Footer />
    </Container>
  )
}

export default Cart
