import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import {useReducer, useEffect} from 'react';
import {  useSelector }from "react-redux"

import { UserProvider } from './UserContext'
import { initialState, reducer} from './reducer/UserReducer'

import Product from "./pages/Product";
import Home from "./pages/Home";
import ProductList from "./pages/ProductList";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Cart from "./pages/Cart";
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import AddProduct from './pages/AddProduct'
import AdminViewUser from './pages/AdminViewUser'


const App = () => {
  // const user = useSelector(state => state.user.currentUser)
  // const navigate = useNavigate()

  const [state, dispatch] = useReducer(reducer)

  return (
    <UserProvider value={{ state, dispatch}}>
      <BrowserRouter>
        <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/products/" element={<ProductList/>}/>
        <Route path="/products/:category" element={<ProductList/>}/>
        <Route path="/product/:id" element={<Product/>}/>  
        {/* <Route path="/Register" element= {user ? <Navigate to="/" /> : <Register />}/>  

        <Route path="/Login" element= {user ? <Navigate to="/" /> : <Login />} /> */}
        <Route path="/register" element={<Register/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="/logout" element={<Logout/>}/>
        <Route path="/addproduct" element={<AddProduct/>}/>
        <Route path="/adminviewuser" element={<AdminViewUser/>}/>
        <Route path="*" element={<ErrorPage/>}/>
   
          

      
           


        <Route path="/Cart" element={<Cart/>}/>  
          
        </Routes>
      </BrowserRouter>
      </UserProvider>
  );
};

export default App;
