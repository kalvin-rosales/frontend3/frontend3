import { Badge } from '@material-ui/core'
import {  ShoppingCartOutlined } from "@material-ui/icons";
import React from 'react'
import {useContext , Fragment} from 'react'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { Nav} from 'react-bootstrap'
import UserContext from './../UserContext'
const token = localStorage.getItem('token')



const Container = styled.div`
    height:60px;
    
    

   
`
const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    allign-items:center;
    justify-content: space-between;
    
    
`

const Left = styled.div`
    flex: 1;
    display: flex;
    allign-items: center;
    
`




const Center = styled.div`
    flex: 1;
    display: flex;
    justify-content: flex-end;
    marginbottom: 5px;
    allign-item: center;
    
    
`

const Banner = styled.h1`
    font-weight:bold;
`

const Right = styled.div`
    flex: 1;
    display: flex;
    allign-item: center;
    justify-content: flex-end;
`
const DisAppear = styled.div`
    font-size:16px;
    cursor:pointer;
    margin-left: 25px
`
const MenuItem = styled.div`
    font-size:16px;
    cursor:pointer;
    margin-left: 25px
`



const Navbar = () => {

    const quantity = useSelector(state=> state.cart.quantity)
    console.log(quantity)
    const { state, dispatch } = useContext(UserContext)


	const NavLinks = () => {

		if(state === true){
			return(
				<Fragment>
					<Nav.Link 
						href="/logout" 
						className="text-dark">Logout</Nav.Link>
				</Fragment>
			)
		} else {
			return(
				<Fragment>
				   <Nav.Link 
						href="/login" 
						className="text-dark">Login</Nav.Link>
				    <Nav.Link 
						href="/register" 
						className="text-dark">Register</Nav.Link>
				</Fragment>
			)
		}
	}

    


  return (
    <Container>
        <Wrapper>
            <Left>
            <Nav.Link 
			href="/" className="text-dark">  <MenuItem>HOME</MenuItem></Nav.Link>
               <Nav.Link 
			href="/products" className="text-dark">
                <MenuItem>PRODUCTS</MenuItem>
                </Nav.Link>
            </Left>
            <Center><Banner>Capstone 3 Eccomerce Rosales</Banner></Center>
            <Right>
                

                <NavLinks/>
                
                <Link to="/cart">
                <MenuItem>
                <Badge badgeContent={quantity} color="primary">
                    
                    
                      <ShoppingCartOutlined />
                    </Badge>
                
                </MenuItem>
                </Link>

            </Right>
        </Wrapper>
    </Container>
  )
}

export default Navbar
