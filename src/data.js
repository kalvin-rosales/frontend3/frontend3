





export const categories = [
    {
        id:1,
        img: "https://png.pngitem.com/pimgs/s/11-118952_chi-chi-chong-nike-sb-hd-png-download.png",
        title: "Nike",
        cat:"Nike"
    },
    {
        id:2,
        img:  "https://png.pngitem.com/pimgs/s/218-2188815_puma-thunder-electric-puma-thunder-electra-hd-png.png",
        title:  "Puma",
        cat:"Puma"
    },
    {
        id:3,
        img:  "https://png.pngitem.com/pimgs/s/47-478000_converse-chuck-70-hi-off-white-converse-chuck.png",
        title:  "Converse",
        cat:"Converse"
    },


]

export const popularProducts = [
    {
        id:4,
        img:"https://www.sneakersnstuff.com/images/74270/product_xsmall.jpg"
    },
    {
        id:5,
        img:"https://photos6.spartoo.eu/photos/215/21544348/21544348_350_A.jpg"
    },
    {
        id:6,
        img:"https://photos6.spartoo.eu/photos/215/21544458/21544458_350_A.jpg"
    },
    {
        id:7,
        img:"https://www.sneakersnstuff.com/images/247076/product_xsmall.jpg"
    },
    {
        id:8,
        img:"https://photos6.spartoo.eu/photos/210/21085421/21085421_350_A.jpg"
    },
    {
        id:9,
        img:"https://photos6.spartoo.eu/photos/188/18801191/18801191_350_A.jpg"
    },
    {
        id:10,
        img:"https://photos6.spartoo.eu/photos/212/21246456/21246456_350_A.jpg"
    },
    {
        id:11,
        img:"https://photos6.spartoo.eu/photos/212/21246447/21246447_350_A.jpg"
    },
]